import React, { useReducer } from "react";
import appContext from "./appContext";
import appReducer from "./appReducer";
import * as types from "./appTypes";

import axiosClient from "../../config/axios";

const AppState = ({ children }) => {
    const initialState = {
        openmodal: false,
    };

    //Crear state y dispatch
    const [state, dispatch] = useReducer(appReducer, initialState);

    return (
        <appContext.Provider
            value={{
                openmodal: state.openmodal,
            }}
        >
            {children}
        </appContext.Provider>
    );
};

export default AppState;
