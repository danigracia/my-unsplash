import * as types from "./appTypes";

const appReducer = (state, action) => {
    switch (action.type) {
        default:
            return state;
    }
};

export default appReducer;
