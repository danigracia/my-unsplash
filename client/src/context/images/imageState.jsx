import React, { useReducer } from "react";
import imageContext from "./imageContext";
import imageReducer from "./imageReducer";
import * as types from "./imageTypes";

import axiosClient from "../../config/axios";

const ImageState = ({ children }) => {
    const initialState = {
        images: [],
        search: [],
        loading: false,
        alert: null,
        addmodal: false,
        deletemodal: false,
        actualimage: null,
    };

    //Crear state y dispatch
    const [state, dispatch] = useReducer(imageReducer, initialState);

    //Get all images
    const getImages = async () => {
        dispatch({
            type: types.GET_IMAGES,
        });

        try {
            const response = await axiosClient("/images");
            dispatch({
                type: types.GET_IMAGES_SUCCESS,
                payload: response.data,
            });
        } catch (error) {
            dispatch({
                type: types.GET_IMAGES_ERROR,
                payload: error.response.data,
            });
        }
    };

    //Search images per name
    const searchImages = async (search) => {
        dispatch({
            type: types.SEARCH_IMAGES,
            payload: search.toLowerCase(),
        });
    };

    //Reset search
    const stopSearch = () => {
        dispatch({
            type: types.SEARCH_STOP,
        });
    };

    //Toggle add image modal
    const toggleAddModal = () => {
        dispatch({
            type: types.TOGGLE_ADD_MODAL,
        });
    };

    //Toggle delete image modal
    const toggleDeleteModal = (id) => {
        dispatch({
            type: types.TOGGLE_DELETE_MODAL,
            payload: id,
        });
    };

    //Add's an image
    const addImage = async (image) => {
        dispatch({
            type: types.ADD_IMAGE,
        });

        try {
            const response = await axiosClient.post("/images", image);
            dispatch({
                type: types.ADD_IMAGE_SUCCESS,
                payload: response.data,
            });
        } catch (error) {
            dispatch({
                type: types.ADD_IMAGE_ERROR,
                payload: error.response.data,
            });
        }
    };

    //Set alert
    const setAlert = (alert) => {
        dispatch({
            type: types.SET_ALERT,
            payload: alert,
        });

        setTimeout(() => {
            dispatch({
                type: types.CLEAR_ALERT,
            });
        }, 3000);
    };

    //Delete's a image
    const deleteImage = async () => {
        dispatch({
            type: types.DELETE_IMAGE,
        });

        try {
            const response = await axiosClient.delete(
                `/images/${state.actualimage}`
            );
            dispatch({
                type: types.DELETE_IMAGE_SUCCESS,
            });
        } catch (error) {
            dispatch({
                type: types.DELETE_IMAGE_ERROR,
                payload: error.response.data,
            });
        }
    };

    return (
        <imageContext.Provider
            value={{
                images: state.images,
                search: state.search,
                loading: state.loading,
                alert: state.alert,
                addmodal: state.addmodal,
                deletemodal: state.deletemodal,
                actualimage: state.actualimage,

                /* Functions */
                getImages,
                searchImages,
                stopSearch,
                toggleAddModal,
                toggleDeleteModal,
                addImage,
                setAlert,
                deleteImage,
            }}
        >
            {children}
        </imageContext.Provider>
    );
};

export default ImageState;
