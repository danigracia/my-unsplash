import * as types from "./imageTypes";

const imageReducer = (state, action) => {
    switch (action.type) {
        case types.GET_IMAGES:
        case types.ADD_IMAGE:
        case types.DELETE_IMAGE:
            return {
                ...state,
                loading: true,
            };

        case types.GET_IMAGES_SUCCESS:
            return {
                ...state,
                loading: false,
                images: action.payload,
            };

        case types.ADD_IMAGE_SUCCESS:
            return {
                ...state,
                loading: false,
                images: [action.payload, ...state.images],
            };

        case types.DELETE_IMAGE_SUCCESS:
            return {
                ...state,
                loading: false,
                images: state.images.filter(
                    (image) => image._id != state.actualimage
                ),
                actualimage: null,
            };

        case types.SEARCH_IMAGES:
            return {
                ...state,
                search: state.images.filter(
                    (image) =>
                        image.label.toLowerCase().indexOf(action.payload) > -1
                ),
            };

        case types.SEARCH_STOP:
            return {
                ...state,
                search: [],
            };

        //Toggle modal
        case types.TOGGLE_ADD_MODAL:
            return {
                ...state,
                addmodal: !state.addmodal,
            };

        case types.TOGGLE_DELETE_MODAL:
            return {
                ...state,
                deletemodal: !state.deletemodal,
                actualimage: action.payload,
            };

        //ERRORS
        case types.GET_IMAGES_ERROR:
        case types.ADD_IMAGE_ERROR:
        case types.ELETE_IMAGE_ERROR:
            return {
                ...state,
                loading: false,
                alert: {
                    type: "error",
                    msg: action.payload,
                },
            };

        //ALERT
        case types.SET_ALERT:
            return {
                ...state,
                alert: action.payload,
            };

        case types.CLEAR_ALERT:
            return {
                ...state,
                alert: null,
            };

        default:
            return state;
    }
};

export default imageReducer;
