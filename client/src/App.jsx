import Index from "./pages/Index";

import ImageState from "./context/images/imageState";
import AppState from "./context/app/appState";

const App = () => {
    return (
        <AppState>
            <ImageState>
                <Index />
            </ImageState>
        </AppState>
    );
};

export default App;
