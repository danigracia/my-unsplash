import { useContext, useEffect, useState } from "react";
import imageContext from "../context/images/imageContext";

//Components
import Search from "../components/Search";
import Galery from "../components/Galery";
import Spinner from "../components/ui/Spinner";

//Images
import logo from "../svg/logo.svg";
import AddImage from "../components/AddImage";
import DeleteImage from "../components/DeleteImage";
const Index = () => {
    const ImageContext = useContext(imageContext);
    const { loading, addmodal, deletemodal, getImages, toggleAddModal } =
        ImageContext;

    useEffect(() => {
        getImages();
    }, []);

    return (
        <div className="container mx-auto my-10 w-11/12">
            <header className="flex flex-col items-center gap-4 md:flex-row md:justify-between">
                <div className="flex flex-col items-center gap-2 md:flex-row md:justify-between md:w-1/3">
                    <img src={logo} alt="Logo" className="w-1/3" />
                    <Search />
                </div>
                <button
                    type="button"
                    className="p-3 bg-emerald-500 rounded-lg text-white font-bold text-lg"
                    onClick={toggleAddModal}
                >
                    Add a photo
                </button>
            </header>
            <main className="mt-10">{loading ? <Spinner /> : <Galery />}</main>

            {/* Modal */}
            {addmodal && <AddImage />}
            {deletemodal && <DeleteImage />}
        </div>
    );
};

export default Index;
