import { useContext, useState } from "react";
import imageContext from "../context/images/imageContext";

import Alert from "./ui/Alert";

const AddImage = () => {
    const [image, setImage] = useState({
        label: "",
        url: "",
    });

    const handleChange = (e) => {
        setImage({
            ...image,
            [e.target.name]: e.target.value,
        });
    };

    const ImageContext = useContext(imageContext);
    const { alert, addImage, toggleAddModal, setAlert } = ImageContext;

    const handleSubmit = (e) => {
        e.preventDefault();

        //VALIDAR FORM
        if (image.label == "" || image.url == "") {
            setAlert({
                type: "error",
                msg: "All fields are required",
            });
            console.log(alert);
            return;
        }

        //Close modal
        toggleAddModal();

        //Add Image
        addImage(image);
    };

    return (
        <div className="fixed top-0 right-0 bottom-0 left-0 bg-gray-900 bg-opacity-70 flex items-center justify-center overflow-hidden">
            <form
                className="bg-white p-6 rounded-lg w-11/12 sm:w-3/4 md:w-2/5 lg:w-1/4"
                onSubmit={handleSubmit}
            >
                <h2 className="text-xl">Add a new photo</h2>

                {alert && <Alert alert={alert} />}

                <div className="mt-4">
                    <label htmlFor="label" className="text-sm text-gray-700">
                        Label
                    </label>
                    <input
                        type="text"
                        name="label"
                        className="block w-full mt-1 p-2 rounded-lg border-2 border-gray-400"
                        placeholder="Suspendisse elit massa"
                        value={image.label}
                        onChange={handleChange}
                    />
                </div>
                <div className="mt-4">
                    <label htmlFor="url" className="text-sm text-gray-700">
                        Photo URL
                    </label>
                    <input
                        type="text"
                        name="url"
                        className="block w-full mt-1 p-2 rounded-lg border-2 border-gray-400"
                        placeholder="https://images.unsplash.com/photo-1471897488648-5eae4ac6686b?"
                        value={image.url}
                        onChange={handleChange}
                    />
                </div>
                <div className="flex justify-end gap-4 mt-4">
                    <button
                        type="button"
                        className="text-gray-500"
                        onClick={toggleAddModal}
                    >
                        Cancel
                    </button>
                    <button
                        type="submit"
                        className="p-3 bg-emerald-500 rounded-lg text-white font-bold"
                    >
                        Submit
                    </button>
                </div>
            </form>
        </div>
    );
};

export default AddImage;
