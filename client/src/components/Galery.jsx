import { useContext } from "react";
import imageContext from "../context/images/imageContext";
import Image from "./Image";

const Galery = () => {
    const ImageContext = useContext(imageContext);
    const { images, search } = ImageContext;

    if (images.length < 1) {
        return (
            <p className="text-center text-xl">
                You haven't added anything yet
            </p>
        );
    }

    return (
        <div className="sm:columns-2 md:columns-3 gap-x-4">
            {search.length > 0
                ? search.map((image) => <Image key={image._id} image={image} />)
                : images.map((image) => (
                      <Image key={image._id} image={image} />
                  ))}
        </div>
    );
};

export default Galery;
