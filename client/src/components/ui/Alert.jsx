import React from "react";

const Alert = ({ alert }) => {
    const { type, msg } = alert;

    return (
        <div
            className={`${
                type === "error" ? "bg-red-500" : "bg-green-500"
            } p-3 text-white my-2 rounded-lg`}
        >
            {msg}
        </div>
    );
};

export default Alert;
