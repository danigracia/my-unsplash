import { useContext } from "react";
import imageContext from "../context/images/imageContext";

const DeleteImage = () => {
    const ImageContext = useContext(imageContext);
    const { deleteImage, toggleDeleteModal } = ImageContext;

    const handleDelete = async () => {
        await deleteImage();

        //Hide modal
        toggleDeleteModal();
    };

    return (
        <div className="fixed top-0 right-0 bottom-0 left-0 bg-gray-900 bg-opacity-70 flex items-center justify-center overflow-hidden">
            <div className="bg-white p-6 rounded-lg w-11/12 sm:w-3/4 md:w-2/5 lg:w-1/4">
                <h2 className="text-2xl text-center">Are you sure?</h2>

                <p className="text-center mt-2 text-gray-700">
                    You won't be able to revert this!
                </p>

                <div className="flex justify-end gap-4 mt-4">
                    <button
                        type="button"
                        className="text-gray-500"
                        onClick={toggleDeleteModal}
                    >
                        Cancel
                    </button>
                    <button
                        type="button"
                        className="p-3 bg-red-500 rounded-lg text-white font-bold"
                        onClick={handleDelete}
                    >
                        Delete
                    </button>
                </div>
            </div>
        </div>
    );
};

export default DeleteImage;
