import { useContext } from "react";
import imageContext from "../context/images/imageContext";

const Image = ({ image }) => {
    const { _id, label, url } = image;

    const ImageContext = useContext(imageContext);
    const { toggleDeleteModal } = ImageContext;

    return (
        <div className="rounded-2xl overflow-hidden mt-4 relative">
            {/* Hover */}
            <div className="opacity-0 hover:opacity-100 absolute top-0 right-0 bottom-0 left-0 bg-black bg-opacity-70 flex items-end p-6 transition-opacity duration-300">
                <button
                    type="button"
                    className="px-2 py-1 text-md md:text-lg text-red-500 border-2 border-red-500 rounded-lg bg-transparent text-sm absolute top-4 right-4 hover:bg-red-500 hover:text-white"
                    onClick={() => toggleDeleteModal(_id)}
                >
                    Delete
                </button>
                <p className="text-white font-bold text-lg md:text-xl">
                    {label}
                </p>
            </div>
            <img src={url} alt={label} />
        </div>
    );
};

export default Image;
