import { useState, useEffect, useContext } from "react";
import imageContext from "../context/images/imageContext";

const Search = () => {
    const ImageContext = useContext(imageContext);
    const { searchImages, stopSearch } = ImageContext;

    const [search, setSearch] = useState("");

    useEffect(() => {
        if (search) {
            searchImages(search);
        } else {
            stopSearch();
        }
    }, [search]);

    return (
        <div className="border-2 border-gray-300 bg-white rounded-lg px-4 py-2 flex items-center text-gray-300">
            <label htmlFor="search" className="mr-2">
                <i className="fas fa-search"></i>
            </label>
            <input
                className="focus:outline-none text-gray-600"
                type="search"
                name="search"
                placeholder="Search by name"
                value={search}
                onChange={(e) => setSearch(e.target.value)}
            />
        </div>
    );
};

export default Search;
