const mongoose = require("mongoose");

const imageSchema = mongoose.Schema({
    label: {
        type: String,
        required: true,
        trim: true,
    },
    url: {
        type: String,
        required: true,
        trim: true,
    },
    created_at: {
        type: Date,
        default: Date.now(),
    },
});

module.exports = mongoose.model("Image", imageSchema);
