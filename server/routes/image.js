const router = require("express").Router();
const { check } = require("express-validator");
const imageController = require("../controllers/imageController");

router.post(
    "/",
    [
        check("label", "The image label is required").not().isEmpty(),
        check("url", "The image url is invalid").isURL(),
    ],
    imageController.createImage
);

router.get("/", imageController.getAllImages);

router.delete("/:id", imageController.deleteImage);

module.exports = router;
