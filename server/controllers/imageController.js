const Image = require("../models/Image");
const { validationResult } = require("express-validator");

exports.createImage = async (req, res) => {
    //Mostrar errores de express validator
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(422).json({ errors: errors.array() });
    }

    try {
        //Crear plato
        const image = await new Image(req.body);
        image.save();

        res.json(image);
    } catch (error) {
        console.log(error);
        res.status(500).send("Internal server error");
    }
};

exports.getAllImages = async (req, res) => {
    try {
        const images = await Image.find({}).sort({ created_at: "asc" });

        res.json(images);
    } catch (error) {
        console.log(error);
        res.status(500).send("Internal server error");
    }
};

exports.deleteImage = async (req, res) => {
    //Check if the image exists
    const image = Image.find({ _id: req.params.id });
    if (!image) {
        return res.status(404).send("Not found");
    }

    try {
        await Image.findOneAndDelete({ _id: req.params.id });
        res.json({ msg: "Image deleted succesfully" });
    } catch (error) {
        console.log(error);
        res.status(500).send("Internal server error");
    }
};
